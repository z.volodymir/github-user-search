import { ReactComponent as SearchIcon } from 'assets/icon-search.svg';
import { Button } from 'components/Button';

import styles from './Search.module.scss';

interface SearchProps {
  hasError: boolean;
  onSubmit: (text: string) => void;
}

type FormFields = {
  username: HTMLInputElement;
};

export const Search = ({ hasError, onSubmit }: SearchProps) => {
  const handleSubmit = (
    event: React.FormEvent<HTMLFormElement & FormFields>
  ) => {
    event.preventDefault();
    const text = event.currentTarget.username.value;

    if (text.trim()) {
      onSubmit(text);
      event.currentTarget.reset();
    }
  };

  return (
    <form onSubmit={handleSubmit} autoComplete="off" className={styles.form}>
      <label htmlFor="search" className={styles.label}>
        <SearchIcon />
      </label>
      <input
        type="search"
        id="search"
        name="username"
        className={styles.search}
        placeholder="Search GitHub username..."
      />
      {hasError && <p className={styles.error}>No result</p>}
      <Button>Search</Button>
    </form>
  );
};
