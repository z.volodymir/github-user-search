import { LocalGithubUser } from 'types';
import styles from './UserStat.module.scss';

export interface UserStatProps
  extends Pick<LocalGithubUser, 'repos' | 'followers' | 'following'> {}

export const UserStat = ({ repos, followers, following }: UserStatProps) => (
  <div className={styles.stat}>
    <div className={styles.info}>
      <div className={styles.title}>Repos</div>
      <div className={styles.number}>{repos}</div>
    </div>
    <div className={styles.info}>
      <div className={styles.title}>Following</div>
      <div className={styles.number}>{following}</div>
    </div>
    <div className={styles.info}>
      <div className={styles.title}>Followers</div>
      <div className={styles.number}>{followers}</div>
    </div>
  </div>
);
