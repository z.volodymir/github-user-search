import { LocalGithubUser } from 'types';

export const defaultUser: LocalGithubUser = {
  "login": "jordwalke",
  "avatar": "https://avatars.githubusercontent.com/u/977348?v=4",
  "name": "Jordan W",
  "company": "Facebook, ReactJS",
  "blog": "https://twitter.com/jordwalke",
  "location": '',
  "bio": '',
  "twitter": '',
  "repos": 124,
  "followers": 8108,
  "following": 44,
  "created": "2011-08-13T04:37:07Z",
}